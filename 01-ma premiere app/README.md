# Pas à pas - Ma première application iOS

Bonjour et bienvenue sur ce billet, premier d'une série en nombre indéterminé.

L'objectif de ce billet - cours - tutoriel, est de démontrer à quel point il est simple de faire une première application pour iPhone (ou iPad).

Grâce à une app très simple, vous découvrirez les principes de base de la conception sur iOS et l'utilisation de Xcode.
Vous pourrez ensuite les appliquer pour développer vos propres applications.

## Objectifs

Pour ma première application, je souhaite pouvoir faire une liste de courses.
Il me faut donc un champ texte, un bouton, et une zone d'affichage.

Je souhaite pouvoir ajouter des éléments les uns à la suite des autres.
Il faut également un bouton de remise à zéro de ma liste.

Ok ce n'est ni sexy, ni révolutionnaire, mais comme je l'ai indiqué en introduction, l'objectif est de se familiariser avec l'interface et les concepts de Xcode.

## Les outils nécessaires

 - De quoi écrire sur un support physique (papier, tableau, ...)

C'est indispensable pour poser ses idées au clair, pouvoir prendre du recul et ne pas foncer tête baissée dans le code.
C'est d'autant plus important quand on commence à travailler en équipe ou quand on souhaite revenir sur une idée après quelques jours/semaines.

Même si notre app est simple, c'est très important de prendre de bonnes bases dès le début.

 - Un mac avec Xcode.
 - Idéalement un iPhone pour tester mais le simulateur permet de débuter sans devoir acheter un téléphone supplémentaire.
 - Une idée
 - Du temps

# Travail préparatif

## Concevoir son application sur papier

Cela permet d'avoir un support toujours visible à portée de main, sur lequel on peut griffonner des idées facilement.
On peut également se mettre à plusieurs autour du support et chacun peut y aller de son grain de sel.

![whiteboard]

## Mise en place de l'environnement de travail

Lors du lancement de Xcode, nous avons cette fenêtre qui s'affiche.

![xcode-launch]

Il faut choisir sur la gauche **Create a new Xcode project**

Cela va lancer l'assistant qui se déroule comme ceci :

![xcode-template]

Choisissez **Single View App**

![xcode-project]

Entrez tous les détails de votre application.

![xcode-done]

Et voilà ! Votre projet est créé.

# Dans le vif du sujet

Maintenant que notre environnement de travail est prêt, nous allons pouvoir continuer.
Avant de nous attaquer au positionnement des différents éléments, nous allons lister les fichiers de notre projet.
Ces fichiers sont visibles sur la gauche de l'écran précédent.

Pour commencer, nous travaillerons uniquement avec les fichiers suivants :
 - `ViewController.swift` : Code de votre interface graphique
 - `Main.storyboard` : interface graphique de l'application

Les autres fichiers ne seront pas modifiés dans notre exemple, mais à titre informatif, voici leur utilité :
 - `AppDelegate` : Code global de votre application (exécuté par exemple au lancement ou à la fermeture de l'app par iOS)
 - `Assets.xcassets` : catalogue d'éléments graphiques (icônes, images, polices, ...)
 - `LanchScreen.storyboard` : Interface graphique du lancement de l'app
 - `Info.plist` : Fichier de configuration

## Positionner les éléments

Plaçons nous sur le fichier `Main.storyboard` en cliquant **une seule fois** dessus.
Pour simplifier, nous dirons simplement `Storyboard` par la suite.

![storyboard]

En bas, vous pouvez voir **View as: iPhone 8**.
En cliquant dessus, ça ouvre le choix des modèles disponibles.

![storyboard-choose-model]

Je vais conserver iPhone 8 pour qu'il reste sur mon écran en totalité. Mais libre à vous de prendre le modèle de votre choix.

Nous allons ouvrir les librairies pour afficher les éléments que nous pouvons utiliser.
Pour cela il faut cliquer sur le bouton rond contenant un carré comme indiqué ici (ou faire `⌘ + Maj+ L`) :

![lib-popup]

Nous allons chercher *Label*, puis nous allons le glisser sur l'application et nous allons le dimensionner à notre guise.

![lib-label]

Et faire de même avec un bouton (*Button*).

![lib-button]

Voila le résultat.

![story-simple]

Pour changer le texte, il existe plusieurs méthodes.
La plus simple est de double cliquer sur l'élément et de saisir le texte désiré.

![view-change-text]

Nous allons pour la première fois lancer le simulateur.
Il faut faire attention a choisir le simulateur qui correspond au modele utilisé pour la conception.
Et cliquer sur la flèche (ou faire `⌘ + B`).

![simu-modele]

Le premier lancement peut être long car il doit démarrer le simulateur et iOS, ce démarrage ne sera fait qu'une fois si vous gardez votre simulateur ouvert entre vos essais.
Les prochains fois ou l'on appuiera sur la flèche, ça va simplement mettre l'application a jour sur le simulateur et la relancer.

![simu-loading]

![simu-launched]

Bon, par contre à ce stade, l'application ne fait rien d'interessant.

Nous allons essayer de faire en sorte que lorsque nous touchons le bouton, un texte prédéfini (*Bonjour !*) s'affiche dans le label.

## Le code

Afin d'y voir plus clair, on va ouvrir côte à côte le storyboard et le code.
Rien de plus simple, il faut cliquer sur le bouton avec des cercles comme des alliances en haut.

![assistant-editor]

Pour y voir plus clair, on peut également fermer les volets de gauche, droite et du bas avec les boutons de droite.

![close-tabs]

![xcode-ready]

### IBOutlet, IBAction

#### IBOutlet

Un IBOutlet est une référence dans le code d'un objet de la vue. Ou autrement dit, une variable qui va pointer vers un élément graphique.

Pour créer un IBOutlet, il faut appuyer sur la touche `control` et glisser de la vue au code.
Une ligne va se dessiner. Une fois qu'on relâche la souris (on peut également relâcher la touche `control`), il faut nommer notre IBOutlet avec un nom clair.
Dans mon exemple, `myLabel`.

![outlet-create]

On peut voir que la ligne est précédée d'un rond, rempli, qui signifie que la liaison est établie avec la vue.
Désormais dans mon code, si je veux interagir avec le label, il me suffit d'utiliser la variable `myLabel`.

![outlet-done]

Par exemple, nous souhaitons changer sa valeur au chargement de l'application.
Sans entrer dans les détails pour le moment, nous pouvons utiliser la méthode `viewDidLoad` qui est lancée par iOS quand la vue est chargée.

![outlet-example]

Relançons l'application et voila le résultat.

![outlet-build]

#### IBAction

Une IBAction, est une méthode qui va être liée à un évènement de la vue. (clic sur un bouton par exemple).

Pour créer une IBAction, c'est aussi très simple.
De la même façon qu'on a créé l'IBOutlet, on va faire un `control` glisser sur le code et la fenêtre n'est plus exactement la même.
Cette fois ci, "connection" est de type "Action", il y a un peu plus de détails mais nous les verrons plus tard en revue.

![ibaction-create]

De la meme facon qu'on a nommé notre label, on va nommer l'action.

![ibaction-done]

Une nouvelle fonction est générée.
Nous pouvons de nouveau modifier le texte de notre myLabel, relancer l'application et une fois qu'on clic sur le bouton, le label change de texte.

![ibaction-code]

![ibaction-simu]

## Notre bloc note

Tout ceci est bien beau, mais cela ne ressemble pas au schéma du tableau blanc du début.
Certes, mais vous avez maintenant les bases pour la suite.

### l'interface

Je vais maintenant ajouter mes éléments et les positionner pour coller à mon schéma.
 - Un champs de saisie utilisateur est un `textField`
 - Un champs de texte multi-ligne est une `textView`

![notepad-textfield]

Une fois notre interface en place, on va réfléchir aux IBOutlets et IBActions nécessaires.

![notepad-view]

### Outlets et Actions

Nous avons besoin de connaitre le texte saisi, et le label qui va se mettre à jour.
Le label nous l'avons déjà donc il faut faire l'Outlet pour le `textField`.

Pour les actions, nous avons deja l'action du bouton "+", c'est le bouton "dire bonjour" que j'ai déplacé et modifié.
Il faut une action pour nettoyer le label donc sur le bouton clear.

![notepad-code-empty]

### La logique

Afin de gagner du temps il est utile de préparer la logique métier en langage humain.

Voici donc la logique de mon application :
 - Quand je clic sur "+"
  - Je souhaite ajouter une ligne dans le textView avec la valeur du textField.
  - Je souhaite également effacer le champs textField


 - Quand je clic sur "clear"
  - Je souhaite effacer tout le label

#### Quand je clic sur "+"

Nous avons vu que pour définir une valeur à un element, il faut faire quelque chose comme

` myTextView.text = "" `

Pour ajouter du texte, il faut concatener (mettre à la suite) plusieurs choses.

 - La chaine deja existante
 - Un retour à la ligne
 - Le nouveau texte

Cela se traduit par :

` myTextView.text = myTextView.text! + "\n" + myTextField.text!`

La petite subtilité du "!" sera expliquée dans un autre cours. Pour faire simple, cela veut dire que la valeur ne peut pas être `null` ou inexistante.

Pour remettre à zéro le `textField`, il faut simplement lui mettre son attribut `text` à une chaine vide.
` myTextField.text = "" `

Et voila une belle liste pour les courses.

#### Quand je clic sur "clear"

Je pense que vous l'aurez compris, il faut vider le textView, comme au chargement.
` myTextView.text = "" `

#### Le code final####

![notepad-code]

# La mise en forme

Pour le moment nous avons travaillé sur un seul modèle d'iPhone.
Que se passe t'il si je souhaite lancer le simulateur iPhone XS ou iPhone 5 ?

Et bien changeons le modèle du simulateur et regardons le résultat

![layout]

Et oui ce n'est pas très joli.

Sur le XS c'est encore utilisable, mais sur le 5, on ne voit pas les boutons,
ils sont hors du cadre car la résolution est plus petite que sur ses cousins.

La raison est que nous avons placé les éléments à la main, de façon absolue (et non dynamique).
Le bouton "clear" se trouve toujours à la même distance en pixel du coin haut/gauche de l'écran.
Or, comme la résolution de l'écran n'est pas la même selon le modèle, cela ne marche pas.

Heureusement, il existe pour cela une fonctionnalité qui s'appelle *Auto Layout*.
Mais nous verrons ça prochainement.

*Nous avons également un cours vidéo gratuit [ici](https://www.purplegiraffe.fr/p/1h-pour-creer-votre-premiere-app-iphone-ios12). Si c’était trop facile pour vous et que vous souhaitez pousser votre apprentissage, suivez notre cours complet [ici](https://www.purplegiraffe.fr/p/ios12-swift-bases).*

### Guillaume Aveline
15 années d’expérience au service du développement de site internet.
Guillaume et sa société CODR vous propose son large éventail de services de développement web et mobile.

Drupal, Wordpress, Symfony, iOS, Android... CODR, le développement sur-mesure. 

[whiteboard]: ./assets/whiteboard.jpg

[xcode-launch]: ./assets/xcode-launch.png
[xcode-template]: ./assets/xcode-template.png
[xcode-project]: ./assets/xcode-project.png
[xcode-done]: ./assets/xcode-done.png

[storyboard]: ./assets/storyboard.24.11.png
[storyboard-choose-model]: ./assets/storyboard-choose-model.png

[lib-popup]: ./assets/lib-popup.png
[lib-label]: ./assets/lib-label.png
[lib-button]: ./assets/lib-button.png
[story-simple]: ./assets/story-simple.png

[view-change-text]: ./assets/view-change-text.png

[simu-modele]: ./assets/simu-modele.png
[simu-loading]: ./assets/simu-loading.png
[simu-launched]: ./assets/simu-launched.png

[assistant-editor]: ./assets/assistant-editor.png
[close-tabs]: ./assets/close-tabs.png
[xcode-ready]: ./assets/xcode-ready.png
[outlet-create]: ./assets/outlet-create.png
[outlet-done]: ./assets/outlet-done.png
[outlet-example]: ./assets/outlet-example.png
[outlet-build]: ./assets/outlet-build.png

[ibaction-create]: ./assets/ibaction-create.png
[ibaction-done]: ./assets/ibaction-done.png
[ibaction-code]: ./assets/ibaction-code.png
[ibaction-simu]: ./assets/ibaction-simu.png

[notepad-textfield]: ./assets/notepad-textfield.png
[notepad-view]: ./assets/notepad-view.png
[notepad-code-empty]: ./assets/notepad-code-empty.png
[notepad-code]: ./assets/notepad-code.png

[layout]: ./assets/layout.png
